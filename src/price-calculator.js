// supported browsers >= IE-11

// I think we should avoid hardcode values so there is initial part
// for example we can send ajax request like /api/getPriceInfo

;(function () {
  'use strict';

  const NORMAL_USER = 0;
  const COMPANY_USER = 1;
  const NEW_PRODUCT = 0;
  const OLD_PRODUCT = 1;

  let newProductMarkup = 0;
  let oldProductMarkup = 0;
  let companyUserDiscount = 0;
  let normalUserDiscount = 0;
  let publishedTodayDiscount = 0;

  function priceCalculator(config) {
    newProductMarkup = config.newProductMarkup;
    oldProductMarkup = config.oldProductMarkup;
    companyUserDiscount = config.companyUserDiscount;
    normalUserDiscount = config.normalUserDiscount;
    publishedTodayDiscount = config.publishedTodayDiscount;
  }

  priceCalculator.calculateMarkup = function(productType) {
    return productType === NEW_PRODUCT ? newProductMarkup : oldProductMarkup;
  };

  priceCalculator.calculateDiscount = function(productType, userType, publishedDate) {
    let result = userType === COMPANY_USER ? companyUserDiscount : normalUserDiscount;

    const isPublishedToday =
      publishedDate && publishedDate.toDateString() === new Date().toDateString();

    if (productType === NEW_PRODUCT && isPublishedToday) {
      result += publishedTodayDiscount;
    }

    return result;
  };

  priceCalculator.calculateFinalPrice = function(userType, productType, price, publishedDate) {
    return price
      + priceCalculator.calculateMarkup(productType)
      - priceCalculator.calculateDiscount(productType, userType, publishedDate);
  };

  priceCalculator.types = {
    NORMAL_USER: NORMAL_USER,
    COMPANY_USER: COMPANY_USER,
    NEW_PRODUCT: NEW_PRODUCT,
    OLD_PRODUCT: OLD_PRODUCT
  };

  window.priceCalculator = priceCalculator;
})();

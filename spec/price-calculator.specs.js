describe('price calculator', function(){
	const	newProductMarkup = 25;
	const	oldProductMarkup = 35;
	const	companyUserDiscount = 5;
	const	normalUserDiscount = 0;
	const	publishedTodayDiscount = 10;

  priceCalculator({
    newProductMarkup: newProductMarkup,
    oldProductMarkup: oldProductMarkup,
    companyUserDiscount: companyUserDiscount,
    normalUserDiscount: normalUserDiscount,
    publishedTodayDiscount: publishedTodayDiscount
  });

  describe('calculateMarkup', function () {
    it('should calculate new product markup', function () {
    	const productType = priceCalculator.types.NEW_PRODUCT;
      const markup = priceCalculator.calculateMarkup(productType);
      expect(markup).to.equal(newProductMarkup);
    });

    it('should calculate old product markup', function () {
      const productType = priceCalculator.types.OLD_PRODUCT;
      const markup = priceCalculator.calculateMarkup(productType);
      expect(markup).to.equal(oldProductMarkup);
    });
  });

  describe('calculateDiscount', function () {
    const publishedDate = new Date(2019, 1, 1);

    it('should calculate discount for normal users', function () {
    	const user = priceCalculator.types.NORMAL_USER;
      const discount = priceCalculator.calculateDiscount(undefined, user, publishedDate);
      expect(discount).to.equal(normalUserDiscount);
    });

    it('should calculate discount for company users', function () {
    	const user = priceCalculator.types.COMPANY_USER;
      const discount = priceCalculator.calculateDiscount(undefined, user, publishedDate);
      expect(discount).to.equal(companyUserDiscount);
    });

    it('should calculate discount for new product if it published today', function () {
      const productType = priceCalculator.types.NEW_PRODUCT;
      const publishedDate = new Date();
      const discount = priceCalculator.calculateDiscount(productType, undefined, publishedDate);
      expect(discount).to.equal(publishedTodayDiscount);
    });
  });

  describe('calculateFinalPrice', function () {
    const calculateMarkup = priceCalculator.calculateMarkup;
    const calculateDiscount = priceCalculator.calculateDiscount;

    it('should return price plus markup minus discount ', function(){
      priceCalculator.calculateMarkup = function() { return 30 };
      priceCalculator.calculateDiscount = function() { return 10 };

      const price = 100;
      const finalPrice = priceCalculator.calculateFinalPrice(undefined, undefined, price, new Date());
      expect(finalPrice).to.equal(120);
    });

    priceCalculator.calculateMarkup = calculateMarkup;
    priceCalculator.calculateDiscount = calculateDiscount
  });
});